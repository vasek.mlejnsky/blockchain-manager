import AppHome from '@/components/AppHome';
import NewWallet from '@/components/NewWallet';

export default
[
  {
    path: '/',
    name: 'AppHome',
    component: AppHome,
    children: [
      {
        path: 'new-wallet',
        name: 'NewWallet',
        component: NewWallet,
      },
    ],
  },
];
